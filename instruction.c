#include "push_swap.h"

void	s_ab(t_stack *stack)
{
	t_node	*tmp;
	int 	save1;
	int 	save2;

	tmp = stack->head;
	if (stack->size > 1)
	{
		save1 = tmp->data;
		tmp = tmp->next;
		save2 = tmp->data;
		tmp->data = save1;
		tmp = tmp->prev;
		tmp->data = save2;
	}
    ft_putchar('p');
    ft_putchar(stack->prop->name);
    ft_putchar('\n');
}

void	ss(t_stack *stack_a, t_stack *stack_b)
{
	s_ab(stack_a);
	s_ab(stack_b);
    ft_putstr("ss\n");
}

void	p_ab(t_stack *stack_a, t_stack *stack_b, int range)
{
	int 			tmp;
	t_properties	*counters;
	
	counters = stack_a->prop;
	if (stack_a->size !=0)
	{
		tmp = popfront(stack_a);
		pushfront(stack_b, tmp, range);
	}
	counters->count_push--;
    ft_putchar('p');
    ft_putchar(stack_a->prop->name);
    ft_putchar('\n');
}

void	r_ab(t_stack *stack, int range)
{
	int 			tmp;
	t_properties	*counters;
	
	counters = stack->prop;
	tmp = popfront(stack);
	pushback(stack, tmp, range);
	counters->count_rot++;
	counters->count_revrot--;
    ft_putchar('r');
    ft_putchar(stack->prop->name);
    ft_putchar('\n');
}

void	rr(t_stack *stack_a, t_stack *stack_b, int range)
{
	r_ab(stack_a, range);
	r_ab(stack_b, range);
    ft_putstr("rr\n");
}