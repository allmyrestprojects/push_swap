NAME = push_swap

SRCS = ./srcs/

SRC = stack_utils.c stack_utils2.c main.c test_utils.c instruction.c instruction2.c simplelogic.c midpoint.c

SRCO = stack_utils.o stack_utils2.o main.o test_utils.o instruction.c instruction2.o simplelogic.o midpoint.o
all: $(NAME)

$(NAME):
	make -C libft/ fclean && make -C libft/
	gcc  -I libft/includes -c $(SRC)
	gcc -o $(NAME) $(SRCO) -I libft/includes -L libft/ -lft

clean:
	rm -f *.o
	rm -rf ./libft/*.o
	rm -rf ./libft/libft.a

fclean: clean
	rm -f $(NAME)

re: fclean all