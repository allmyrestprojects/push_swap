cmake_minimum_required(VERSION 3.14)
project(push_swap C)

set(CMAKE_C_STANDARD 99)

add_executable(push_swap
        push_swap.h
        ft_stack.h
        ps_basic_algor.c
        push_swap.c
        ps_pa_while.c
        ps_pb_while.c
        ps_sort_a.c
        ps_sort_top_a.c
        ps_pa_all.c
        ps_rra_while.c
        ps_rrb_while.c
        ps_sort_b.c
        ps_sort_top_b.c
        ft_stckaddback.c
        ft_stckaddtop.c
        ft_stckdel.c
        ft_stckdellast.c
        ft_stckgetmedian.c
        ft_stckinit.c
        ft_stcklen.c
        ft_stckmedianlen.c
        ft_stcknew.c
        ft_stckpop.c
        ft_stckprint.c
        ft_stckpush.c
        ft_stckrevrot.c
        ft_stckrot.c
        ft_stckswap.c
        is_valid_argument.c
        operations.c
        ft_stckissorted.c)

add_subdirectory(libft)

target_link_libraries(push_swap ft)