#include "push_swap.h"

void	fillstack(char **argv, t_stack *stack)
{
    long	value;
    int 	i;

    argv++;
    while(*argv)
    {
        value = ft_atoi(*argv);
        pushback(stack, (int)value, 0);
        argv++;
    }
}

void	block_instr(char *instr, t_stack *stack_a, t_stack *stack_b)
{
    if (ft_strcmp(instr,"pa") == 0)
        p_ab(stack_b, stack_a, 0);
    else if (ft_strcmp(instr,"pb") == 0)
        p_ab(stack_a, stack_b, 0);
    else if (ft_strcmp(instr,"sa") == 0)
        s_ab(stack_a);
    else if (ft_strcmp(instr,"sb") == 0)
        s_ab(stack_b);
    else if (ft_strcmp(instr,"ss") == 0)
        ss(stack_a, stack_b);
    else if (ft_strcmp(instr,"ra") == 0)
        r_ab(stack_a, 0);
    else if (ft_strcmp(instr,"rb") == 0)
        r_ab(stack_b, 0);
    else if (ft_strcmp(instr,"rr") == 0)
        rr(stack_a, stack_b, 0);
    else if (ft_strcmp(instr,"rra") == 0)
        rr_ab(stack_a, 0);
    else if (ft_strcmp(instr,"rrb") == 0)
        rr_ab(stack_b, 0);
    else if (ft_strcmp(instr,"rrr") == 0)
        rrr(stack_a, stack_b, 0);
}

void	get_instruction(char *str, t_stack *stack_a, t_stack *stack_b)
{
    char	instr[4];
    int		i;
    int 	j;

    i = 0;
    j = 0;
    while(str[j] != '\0')
    {
        while (str[j] != ' ')
        {
            instr[i++] = str[j];
            j++;
        }
        instr[i] = '\0';
        block_instr(instr, stack_a, stack_b);
        i = 0;
        j++;
    }
}

int     len_range(t_stack *stack, int range)
{
    int     len;
    t_node  *tmp;

    len = 0;
    tmp = stack->head;
    while (tmp && tmp->range == range)
    {
        len++;
        tmp = tmp->next;
    }
    return (len);
}

void	init_counters(t_stack *stack)
{
	t_node  		*tmp;
	t_properties	*counters;
	
	tmp = stack->head;
	counters = stack->prop;
	counters->count_push = len_range(stack, tmp->range) / 2;
	counters->count_rot = 0;
	counters->count_revrot = 0;
	counters->midpoint = head_part(stack);
}

int     eq(int a, int b)
{
    if (a == b)
        return (1);
    return (0);
}

int     lt(int a, int b)
{
    if ( a < b)
        return (1);
    return (0);
}

int     gt(int a, int b)
{
    if (a > b)
        return (1);
    return (0);
}

void    rotnpush(t_stack *stack_a, t_stack *stack_b, int (*f)(int, int))
{
    t_node          *tmp;
    t_properties    *counters;

    counters = stack_a->prop;
    tmp = stack_a->head;
    while (counters->count_push)
    {
        if (f(tmp->data , counters->var))
            p_ab(stack_a, stack_b, counters->midpoint);
        else
            r_ab(stack_a, tmp->range);
        tmp = stack_a->head;
    }
}

void	rerange(t_stack *stack)
{
	int 	midpoint;
	t_node	*tmp;
	int 	part;

    tmp = stack->head;
    part = tmp->range;
    midpoint = head_part(stack);
    while (tmp && tmp->range == part)
    {
        tmp->range = midpoint;
        tmp = tmp->next;
    }
}

void    revrota(t_stack *stack)
{
    t_properties    *counters;
    t_node          *tmp;

    tmp = stack->tail;
    counters = stack->prop;
    while (counters->stop && counters->count_rot)
    {
        rr_ab(stack, tmp->range);
        tmp = stack->tail;
    }
}

void	push_rot_a(t_stack *stack_a, t_stack *stack_b)
{
    t_properties	*counters;
    t_node          *tmp;

    counters = stack_a->prop;
    counters->count_push = 1;
    counters->var = find_min(stack_a);
    rotnpush(stack_a, stack_b, eq);
    revrota(stack_a);
}

void    pushtob(t_stack *stack_a, t_stack *stack_b)
{
    t_node  		*tmp;
    t_properties	*counters;

    tmp = stack_a->head;
    counters = stack_a->prop;
    while (tmp->data < counters->midpoint && counters->count_push)
    {
        p_ab(stack_a, stack_b, counters->midpoint);
        tmp = stack_a->head;
    }
}

void    first_step(t_stack *stack_a, t_stack *stack_b)
{
    t_properties	*counters;

    rerange(stack_a);
	init_counters(stack_a);
	if (stack_a->size != 0 && len_range(stack_a, stack_a->head->range) == 4)
		push_rot_a(stack_a, stack_b);
	else
	{
	    while (stack_a->size != 0 && len_range(stack_a, stack_a->head->range) > 3)
	    {
            counters = stack_a->prop;
            init_counters(stack_a);
            rerange(stack_a);
            counters->var = counters->midpoint;
            pushtob(stack_a, stack_b);
            rotnpush(stack_a, stack_b, lt);
            revrota(stack_a);
            rerange(stack_b);
        }
	}
	sort_stack(stack_a, asc);
}

void    second_step(t_stack *stack_a, t_stack *stack_b)
{
    t_node  *tmp;

    tmp = stack_b->head;
    if (stack_b->size != 0 && len_range(stack_b, tmp->range) <= 3)
    {
        sort_stack(stack_b, desc);
        pushtoa(stack_b, stack_a);
    }
    else
        half_push(stack_a, stack_b);
    if (stack_a->size !=0 && len_range(stack_a, stack_a->head->range) > 3)
        first_step(stack_a, stack_b);
    else
        sort_stack(stack_a, asc);
    if (stack_b->size != 0 && stack_b->size != len_range(stack_b, stack_b->head->range))
        revrot(stack_a, stack_b);
}

int     head_part(t_stack *stack)
{
    t_node  *tmp;
    t_stack *aux_stack;
    int     part;
    int     midpoint;

    midpoint = 0;
    if (stack)
    {
        aux_stack = init('x');
        tmp = stack->head;
        part = tmp->range;
        while (tmp && tmp->range == part) {
            pushfront(aux_stack, tmp->data, tmp->range);
            tmp = tmp->next;
        }
        midpoint = find_midpoint(aux_stack);
        delete_stack(&aux_stack);
    }
    return (midpoint);
}

void    simplelogic(t_stack *stack_a, t_stack *stack_b)
{
    int 	stop;

    stack_a->prop->stop = 0;
    stop = 1;
    while (stop)
	{
        first_step(stack_a, stack_b);
		rerange(stack_b);
        stack_a->prop->stop = 1;
		second_step(stack_a, stack_b);
		stop = stack_b->size;
	}
}