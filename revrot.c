#include "push_swap.h"

void    revrot(t_stack *stack_a, t_stack *stack_b)
{
    t_node  *tmp;
    int 	tailmidpoint;
    int 	headmidpoint;
    int 	part;
    
    tmp = stack_b->tail;
    part = tmp->range;
    tailmidpoint = tmp->range;
    headmidpoint = stack_b->head->range;
    while (tmp && part == tmp->range && tailmidpoint >= headmidpoint)
    {
        rr_ab(stack_b, tmp->range);
        tmp = stack_b->head;
        if (tmp->data >= tailmidpoint)
            p_ab(stack_b, stack_a, tailmidpoint);
        tmp = stack_b->tail;
    }
}