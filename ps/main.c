#include "push_swap.h"

int main(int argc, char **argv)
{
	t_stack *stack_a;
	t_stack *stack_b;
	
	stack_a = init('a');
	stack_b = init('b');

	fillstack(argv, stack_a);
    /*
	printstack(stack_a);
	printf("stack_b\n");
	printstack(stack_b);
	printf("-------\n");
    */
    simplelogic(stack_a, stack_b);
    /*
	printf("stack_a\n");
	printstack(stack_a);
	printf("stack_b\n");
	printstack(stack_b);
	printf("-------\n");
    */
	delete_stack(&stack_a);
	delete_stack(&stack_b);
	return (0);
}