#include "push_swap.h"

int     desc(int a, int b)
{
    if (a > b)
        return (0);
    return (1);
}

int     asc(int a, int b)
{
    if (a < b)
        return (0);
    return (1);
}

int     is_sort(t_stack *stack, int (*f)(int, int))
{
    t_node  *tmp;
    int     part;

    tmp = stack->head;
    part = tmp->range;
    while(tmp->next && tmp->range == part &&
            tmp->next->range == part)
    {
        if (f(tmp->data, tmp->next->data))
            return (0);
        tmp = tmp->next;
    }
    return (1);
}

void    pushtoa(t_stack *stack_a, t_stack *stack_b)
{
    int		part;
    t_node	*tmp;

    tmp = stack_a->head;
    part = tmp->range;
    while (tmp && tmp->range == part)
    {
        p_ab(stack_a, stack_b, part);
        tmp = stack_a->head;
    }
}

void    sort_stack(t_stack *stack, int (*f)(int, int))
{
    rerange(stack);
    if (!is_sort(stack, f))
    {
        if (len_range(stack, stack->head->range) == 2)
            s_ab(stack);
        else if (len_range(stack, stack->head->range) > 2)
           instr_sort(stack);
    }
}
