#include "push_swap.h"

void    qsortRecursive(int *mas, int size)
{
    int i = 0;
    int j = size - 1;
    int mid = mas[size / 2];

    while (i <= j)
    {
        while(mas[i] < mid)
            i++;
        while(mas[j] > mid)
            j--;
        if (i <= j) {
            int tmp = mas[i];
            mas[i] = mas[j];
            mas[j] = tmp;

            i++;
            j--;
        }
    }
    if(j > 0)
        qsortRecursive(mas, j + 1);
    if (i < size)
        qsortRecursive(&mas[i], size - i);
}

int find_midpoint(t_stack *stack)
{
    int     i;
    int     *arr;
    t_node  *tmp;
    int     midpoint;

    i = -1;
    tmp = stack->head;
    arr = (int*)malloc(sizeof(int) * stack->size);
    while (++i, i < stack->size)
    {
        arr[i] = tmp->data;
        tmp = tmp->next;
    }
    qsortRecursive(arr, stack->size);
    midpoint = arr[stack->size / 2];
    free(arr);
    return (midpoint);
}
