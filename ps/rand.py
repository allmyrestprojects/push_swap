#!/usr/bin/env python

import random
import sys

last = int(sys.argv[1]) + 1
l = list(range(1, last))
random.shuffle(l)
print " ".join(map(str,l))
