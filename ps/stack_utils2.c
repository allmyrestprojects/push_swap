#include "push_swap.h"

t_node*	getnth(t_stack *stack, size_t index)
{
	t_node *tmp;
	
	tmp = NULL;
	size_t i;
	if (index < stack->size / 2)
	{
		i = 0;
		tmp = stack->head;
		while (tmp && i < index) {
			tmp = tmp->next;
			i++;
		}
	}
	else
		{
		i = stack->size - 1;
		tmp = stack->tail;
		while (tmp && i > index)
		{
			tmp = tmp->prev;
			i--;
		}
	}
	return tmp;
}

void	insert(t_stack *stack, size_t index, int value)
{
	t_node *elm = NULL;
	t_node *ins = NULL;
	
	elm = getnth(stack, index);
	if (elm == NULL)
		exit(5);
	ins = (t_node*)malloc(sizeof(t_node));
	ins->data = value;
	ins->prev = elm;
	ins->next = elm->next;
	if (elm->next)
		elm->next->prev = ins;
	elm->next = ins;
	if (!elm->prev)
		stack->head = elm;
	if (!elm->next)
		stack->tail = elm;
	stack->size++;
}

int		deletenth(t_stack *stack, size_t index)
{
	t_node	*elm;
	int 	tmp;

	elm = getnth(stack, index);
	if (elm == NULL)
		exit(5);
	if (elm->prev)
		elm->prev->next = elm->next;
	if (elm->next)
		elm->next->prev = elm->prev;
	tmp = elm->data;
	if (!elm->prev)
		stack->head = elm->next;
	if (!elm->next)
		stack->tail = elm->prev;
	free(elm);
	stack->size--;
	return (tmp);
}

void	printstack(t_stack *stack)
{
	t_node	*tmp;
	
	tmp = stack->head;
	while (tmp)
	{
		printf("%d\t%d\n", tmp->data, tmp->range);
		tmp = tmp->next;
	}
}

