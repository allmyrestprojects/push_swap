#include "push_swap.h"

void	first_a(t_stack *stack)
{
    t_node  *tmp;

    tmp = stack->head;
    if (tmp->data > tmp->next->data)
        s_ab(stack);
    else if (tmp->data < tmp->next->data)
    {
        r_ab(stack, tmp->range);
        s_ab(stack);
        rr_ab(stack, tmp->range);
        s_ab(stack);
    }
}

void	middle_a(t_stack *stack)
{
    t_node  *tmp;

    tmp = stack->head;
    s_ab(stack);
    r_ab(stack, tmp->range);
    s_ab(stack);
    rr_ab(stack, tmp->range);
    s_ab(stack);
}

void	last_a(t_stack *stack)
{
    t_node  *tmp;

    tmp = stack->head;
    if (tmp->data > tmp->next->data)
    {
        s_ab(stack);
        r_ab(stack, tmp->range);
        s_ab(stack);
        rr_ab(stack, tmp->range);
    }
    else if (tmp->data < tmp->next->data)
    {
        r_ab(stack, tmp->range);
        s_ab(stack);
        rr_ab(stack, tmp->range);
    }
}