#ifndef PUSH_SWAP_PUSH_SWAP_H
#define PUSH_SWAP_PUSH_SWAP_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//#include "libft/libft.h"

typedef struct		s_node
{
	int				data;
	int             range;
	struct s_node	*next;
	struct s_node	*prev;
}					t_node;

typedef struct      s_properties
{
    char            name;
    int             range;
    int 			count_push;
    int 			count_rot;
    int 			count_revrot;
    int 			midpoint;
    int 			stop;
    int             var;
}                   t_properties;

typedef struct		s_stack
{
	size_t 			size;
	t_node			*head;
	t_node			*tail;
	t_properties    *prop;
}					t_stack;

t_stack	*init(char name);
void	delete_stack(t_stack **stack);
void	pushfront(t_stack *stack, int value, int range);
int		popfront(t_stack *stack);
void	pushback(t_stack *stack, int value, int range);
int		popback(t_stack *stack);
t_node*	getnth(t_stack *stack, size_t index);
void	insert(t_stack *stack, size_t index, int value);
int		deletenth(t_stack *stack, size_t index);

void	printstacks(t_stack *stack_a, t_stack *stack_b);
void	printstack(t_stack *stack);
void	test_utils();

/* instruction */
void	s_ab(t_stack *stack);
void	ss(t_stack *stack_a, t_stack *stack_b);
void	p_ab(t_stack *stack_a, t_stack *stack_b, int range);
void	r_ab(t_stack *stack, int range);
void	rr(t_stack *stack_a, t_stack *stack_b, int range);
void	rr_ab(t_stack *stack, int range);
void	rrr(t_stack *stack_a, t_stack *stack_b, int range);
void 	fillstack(char **argv, t_stack *stack);
void	block_instr(char *instr, t_stack *stack_a, t_stack *stack_b);
void	get_instruction(char *str, t_stack *stack_a, t_stack *stack_b);
void    divide(unsigned long sa_size, t_stack *stack_a, t_stack *stack_b);
void    divide_even(unsigned long sa_size, t_stack *stack_a, t_stack *stack_b);

int     find_midpoint(t_stack *stack);
int		find_min(t_stack *stack);
void    simplelogic(t_stack *stack_a, t_stack *stack_b);
void    sort_stack(t_stack *stack, int (*f)(int, int));
int     len_range(t_stack *stack, int range);
int     is_sort(t_stack *stack, int (*f)(int, int));
int     asc(int a, int b);
int     desc(int a, int b);
void    pushtoa(t_stack *stack_a, t_stack *stack_b);
int     head_part(t_stack *stack);
void    instr_sort(t_stack *stack);
int     half_push(t_stack *stack_a, t_stack *stack_b);
void    revrot(t_stack *stack_a, t_stack *stack_b);
void	rerange(t_stack *stack);
/* hardcode instr*/
void	first_a(t_stack *stack);
void	middle_a(t_stack *stack);
void	last_a(t_stack *stack);
void	first_b(t_stack *stack);
void	middle_b(t_stack *stack);
void	last_b(t_stack *stack);

/* templates */
int     eq(int a, int b);
int     lt(int a, int b);
int     gt(int a, int b);
void    rotnpush(t_stack *stack_a, t_stack *stack_b, int (*f)(int, int));

/* ft */
int	    ft_atoi(const char *str);
void	ft_putstr(char const *s);
void	ft_putchar(char c);
int	    ft_strcmp(const char *s1, const char *s2);

#endif //PUSH_SWAP_PUSH_SWAP_H