#include "push_swap.h"

void	rr_ab(t_stack *stack, int range)
{
	int 			tmp;
	t_properties	*counters;
	
	counters = stack->prop;
	tmp = popback(stack);
	pushfront(stack, tmp, range);
	counters->count_rot--;
	counters->count_revrot++;
    ft_putstr("rr");
    ft_putchar(stack->prop->name);
    ft_putchar('\n');
}

void	rrr(t_stack *stack_a, t_stack *stack_b, int range)
{
	rr_ab(stack_a, range);
	rr_ab(stack_b, range);
    ft_putstr("rrr\n");
}
